resource "aws_security_group" "sg1" {
  name = "TF-SG"
  description = "This is a WebSG by TF"
  vpc_id = var.vpc-id

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami = "ami-06b263d6ceff0b3dd"
  instance_type = "t2.micro"
}

resource "aws_eip" "eip" {
  instance = aws_instance.web.id
}
