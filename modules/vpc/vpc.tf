#provider "aws" {
#  region = "us-east-1"
#  profile = "ioe-sandbox"
#}

### DATA
data "aws_availability_zones" "zones" {}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_network
  tags = {
    name = "terraform-cert"
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = "terraform-cert"
  }
}
resource "aws_subnet" "subnet" {
  count = var.vpc_subnet
  cidr_block = cidrsubnet(var.vpc_network,8 ,count.index )
  vpc_id = aws_vpc.vpc.id
  #availability_zone_id = data.aws_availability_zones.zones.names[count.index]
  availability_zone = data.aws_availability_zones.zones.names[count.index]
  tags = {
    name = "subnet-${count.index + 1}"
  }
}
resource "aws_route_table" "rt-public" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}
#resource "aws_route_table" "rt-private" {
#  vpc_id = aws_vpc.vpc.id
#  route {
#    cidr_block = aws_subnet.subnet[1].cidr_block
#  }
#}
resource "aws_route_table_association" "rta1" {
  route_table_id = aws_route_table.rt-public.id
  subnet_id = aws_subnet.subnet[0].id
}
#resource "aws_route_table_association" "rta2" {
#  route_table_id = aws_route_table.rt-private.id
#  subnet_id = aws_subnet.subnet[1].id
#}

output "vpc-id" {
  value = aws_vpc.vpc.id
}

output "public" {
  value = aws_subnet.subnet[0].id
}
