variable "vpc_network" {
  type = string
  default = "10.0.0.0/16"
}

variable "vpc_subnet" {
  default = "2"
}
