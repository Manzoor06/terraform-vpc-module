provider "aws" {
  region = "us-east-1"
  profile = "ioe-sandbox"
}

module "vpc" {
  source = "./modules/vpc/"
  vpc_network = "10.1.0.0/16"
#  vpc_subnet = 3
}

module "ec2" {
  source = "./modules/ec2/"
  vpc-id = module.vpc.vpc-id
  subnet-id = module.vpc.public
}

output "vpc-id" {
  value = module.vpc.vpc-id
}

output "subnet" {
  value = module.vpc.public
}
